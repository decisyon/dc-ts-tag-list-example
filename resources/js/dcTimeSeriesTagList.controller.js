(function() {
	'use strict';

	function DCTimeSeriesTagListController($scope, $timeout, $q, $http) {

		$scope.DECISYON.target.registerDataConnector(function(requestor) {
			var predixData = $scope.DECISYON.target.content.ctx.predixIntegration.value,
			uaaAccessToken = predixData.uaaAccessToken,
			zoneId = predixData.zoneId,
			limit = $scope.DECISYON.target.content.ctx.$pxMaxTagsLimit.value,
			deferer = $q.defer(),
			outputData = [];

			$http({
				method	: 'GET',
				url		: 'https://time-series-store-predix.run.aws-usw02-pr.ice.predix.io/v1/tags',
				headers	: {
				'authorization'	: 'Bearer ' + uaaAccessToken,
				'predix-zone-id': zoneId
				}
			}).then(
				function successCallback(response) {
					var i = 0,
					len = response.data.results.length;

					for (i; i < len; i++) {
						if (i < limit){
							var newRow = {};
							newRow.key = i;
							newRow.val = response.data.results[i];
							outputData.push(newRow);
						}else {
							break;
						}
					}
					deferer.resolve(outputData);
				},
				function errorCallback(reject) {
					deferer.reject(reject);
				}
			);
			return {
				data : deferer.promise
			};
		});

	}

	DCTimeSeriesTagListController.$inject = ['$scope', '$timeout', '$q', '$http'];
	DECISYON.ng.register.controller('dcTimeSeriesTagListCtrl', DCTimeSeriesTagListController);

}());