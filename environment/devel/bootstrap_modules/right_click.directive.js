(function() {
  'use strict';

  function RightClickDirective($parse) {
    return function(scope, element, attrs) {
      var fn = $parse(attrs.dcyRightClick);
      element.bind('contextmenu', function(event) {
        scope.$apply(function() {
          event.preventDefault();
          event.stopPropagation();
          fn(scope, {
            $event: event
          });
        });
      });
    };
  }

  RightClickDirective.$inject = ['$parse'];

  angular.module('dcyApp.directives')
   .directive('dcyRightClick', RightClickDirective);

}());
